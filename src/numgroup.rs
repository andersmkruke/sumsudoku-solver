#[derive(Debug, Clone)]
pub struct Numgroup {
   sum: u8,
   indexes: Vec<usize>,
}

impl Numgroup {
   pub fn new(sum: u8, indexes: Vec<usize>) -> Self {
      Numgroup {
         sum,
         indexes,
      }
   }

   pub fn indexes(&self) -> &Vec<usize> {
      &self.indexes
   }

   pub fn add_index(&mut self, index: usize) {
      self.indexes.push(index);
   }

   pub fn sum(&self) -> u8 {
      self.sum
   }

   pub fn set_sum(&mut self, sum: u8) {
      self.sum = sum;
   }
}