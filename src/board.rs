use super::numgroup::Numgroup;
use super::solver::Solver;

use std::collections::HashMap;
use std::fs::File;
use std::io::prelude::*;

pub struct Board {
   numgroups: Vec<Numgroup>,
}

impl Board {
   pub fn new() -> Self {
      let numgroups = vec!(
         Numgroup::new(16, vec!(0, 1, 9)),
         Numgroup::new(12, vec!(2,3,11)),
         Numgroup::new(13, vec!(4,12,13)),
         Numgroup::new(12, vec!(5,14)),
         Numgroup::new(7, vec!(6,15)),
         Numgroup::new(17, vec!(7,8)),
         Numgroup::new(9, vec!(16,17)),
         Numgroup::new(13, vec!(18,27,36)),
         Numgroup::new(9, vec!(10,19)),
         Numgroup::new(18, vec!(20,21,30)),
         Numgroup::new(17, vec!(22,23,24)),
         Numgroup::new(5, vec!(25,26)),
         Numgroup::new(6, vec!(28,29)),
         Numgroup::new(8, vec!(31,40)),
         Numgroup::new(13, vec!(32,33)),
         Numgroup::new(10, vec!(34,35)),
         Numgroup::new(20, vec!(37,38,39,47)),
         Numgroup::new(15, vec!(41,42)),
         Numgroup::new(8, vec!(43,44)),
         Numgroup::new(17, vec!(45,46)),
         Numgroup::new(9, vec!(48,49)),
         Numgroup::new(8, vec!(50,51)),
         Numgroup::new(8, vec!(52,53)),
         Numgroup::new(11, vec!(54,55)),
         Numgroup::new(6, vec!(56,57)),
         Numgroup::new(14, vec!(58,67)),
         Numgroup::new(5, vec!(59,68)),
         Numgroup::new(27, vec!(60,61,69,70,78,79)),
         Numgroup::new(18, vec!(62,71,80)),
         Numgroup::new(10, vec!(63,64)),
         Numgroup::new(10, vec!(65,66)),
         Numgroup::new(11, vec!(72,73)),
         Numgroup::new(15, vec!(74,75)),
         Numgroup::new(8, vec!(76,77))
      );

      Board{
         numgroups,
      }
   }

   pub fn from_file() -> Self {
      let mut file = File::open("test_problem.txt").unwrap();
      let mut contents = String::new();
      file.read_to_string(&mut contents);

      let board = Self::parse_board_string(contents);
      board.verify_board();

      board
   }

   fn parse_board_string(board_string: String) -> Self {
      let tiles: Vec<&str> = board_string.split(',').collect();
      
      let mut index = 0;
      let mut numgroups: HashMap<&str, Numgroup> = HashMap::new();

      for tile in tiles {
         let tokens: Vec<&str> = tile.split('=').collect();
         if tokens.len() == 0 || tokens.get(0) == Some(&"") || tokens.len() > 2 {
            println!("WARNING - Rejected tile: {}", tile);
            continue;
         }

         let key: &str = tokens.get(0).unwrap().clone();
         let value = tokens.get(1).clone();

         if let Some(numgroup) = numgroups.get_mut(key) {
            numgroup.add_index(index);
            if let Some(value) = value {
               numgroup.set_sum(value.parse::<u8>().unwrap());
            }
         } else {
            if let Some(value) = value {
               numgroups.insert(key, Numgroup::new(value.parse::<u8>().unwrap(), vec!(index)));
            }
         }

         index += 1;
      }

      Board {
         numgroups: numgroups.values().cloned().collect(),
      }
   }

   fn verify_board(&self) {
      let mut sum = 0;
      for ng in &self.numgroups {
         sum += ng.sum() as u32;
      }

      if sum != 405 {
         panic!("numgroup sum doesn't equal 405!");
      }
   }

   pub fn numgroups(&self) -> &Vec<Numgroup> {
      &self.numgroups
   }

   pub fn solve(&mut self) {
      let mut solver = Solver::new(&self.numgroups);
      solver.solve();
   }
}