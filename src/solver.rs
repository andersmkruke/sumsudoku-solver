use super::tile::{TestTile, TileBlock};
use super::numgroup::Numgroup;
use std::time::SystemTime;
use std::io;

#[derive(Debug)]
pub struct Solver<'a> {
    board: Vec<TestTile>,
    numgroups: &'a Vec<Numgroup>,
    start_time: SystemTime,
    i: u128,
}

impl<'a> Solver<'a> {
    pub fn new(numgroups: &'a Vec<Numgroup>) -> Self {
        Solver {
            board: Self::create_empty_global_state(),
            numgroups,
            start_time: SystemTime::now(),
            i: 0,
        }
    }

    fn create_empty_global_state() -> Vec<TestTile> {
        let mut board = vec![];
        for i in 0..81 {
            board.push(TestTile::new(i));
        }
        board
    }

    pub fn solve(&mut self) -> bool {
        let success = self.solve_tile(0);
        println!("SOLUTION:     (iterations: {})", self.i);
        self.print();
        success
    }

    // fn check_false(&self, index: usize, current_value: u8, msg: &str) -> bool {
    //     let solution: Vec<u8> = vec!(3,1,2,9,4,5,6,7,8,8,5,7,2,6,3,1,4,9,6,4,9,1,7,8,5,2,3,9,6,4,7,5,1,3,8,2,5,3,8,6,2,9,7,1,4,2,7,1,3,8,4,9,5,6,7,9,6,8,1,2,4,3,5,1,2,5,4,3,6,8,9,7,4,8,3,5,9,7,2,6,1);
    //     let mut b = true;
    //     for i in 0..index {
    //         if self.board.get(i).unwrap().current_value().unwrap() != *solution.get(i).unwrap() {
    //             b = false;
    //         }
    //     }

    //     if current_value != *solution.get(index).unwrap() && current_value != 10 {
    //         b = false;
    //     }

    //     if b {
    //         println!("{}", msg);
    //         // dbg!(&self.board);
    //     }

    //     b
    // }

    fn solve_tile(&mut self, tile_index: usize) -> bool {
        self.i += 1;

        if tile_index >= 81 {
            return true;
        }
        // no possible values is an illegal state
        let mut tile = self.board.get(tile_index).unwrap().clone();
        if tile.possible_values().len() == 0 {
            // println!("ERROR - Possible values of tile is empty (tile_index: {})", tile_index);
            return false;
        }
        
        // run recursively for each possible value
        let values = tile.possible_values().clone();
        let mut blocked_values: Vec<TileBlock> = vec!();
        let numgroup = self.get_numgroup(&tile).unwrap().clone();
        for value in values {
            tile.set_current_value(value);
            if !self.check_numgroup(&numgroup, &tile) {
                continue;
            }

            self.board.get_mut(tile_index).unwrap().set_current_value(value);

            blocked_values = self.block_values(&mut tile);
            if self.solve_tile(tile_index + 1) {
                return true;
            }
            self.unblock_values(&mut blocked_values);
        }

        self.board.get_mut(tile_index).unwrap().clear_current_value();
        // self.check_false(tile_index, 10, "total failure");
        false
    }

    fn get_numgroup(&self, tile: &TestTile) -> Option<&Numgroup> {
        for numgroup in self.numgroups {
            for index in numgroup.indexes() {
                if &tile.index() == index {
                    return Some(numgroup);
                }
            }
        }

        None
    }

    fn check_numgroup(&self, numgroup: &Numgroup, tile: &TestTile) -> bool {
        let mut sum = 0;
        for ng_index in numgroup.indexes() {
           if ng_index == &tile.index() {
               sum += tile.current_value().unwrap();
               continue;
           }
           if self.board.get(*ng_index).unwrap().current_value().is_none() {
               return true;
           } else {
               sum += self.board.get(*ng_index).unwrap().current_value().unwrap();
           }
        }
        
        sum == numgroup.sum()
    }

    fn block_values(&mut self, current_tile: &mut TestTile) -> Vec<TileBlock> {
        let mut blocked = vec!();
        
        blocked.append(&mut self.block_column(current_tile));
        blocked.append(&mut self.block_row(current_tile));
        blocked.append(&mut self.block_square(current_tile));

        blocked
    }

    fn block_tile(&mut self, index: usize, value: u8) -> Option<TileBlock> {
        if self.board.get_mut(index).unwrap().remove_possible_value(value) {
            Some(TileBlock::new(value, index))
        } else {
            None
        }
    }

    fn block_column(&mut self, current_tile: &mut TestTile) -> Vec<TileBlock> {
        let mut blocked = vec!();
        let mut index = current_tile.index();
        index += 9;
        while index < 81 {
            if let Some(block) = self.block_tile(index, current_tile.current_value().unwrap()) {
                blocked.push(block);
            }
            index += 9;
        }

        blocked
    }

    fn block_row(&mut self, current_tile: &mut TestTile) -> Vec<TileBlock> {
        let mut blocked = vec!();
        let index = current_tile.index();

        for offset in (index + 1)..(9 * ((index+9)/9)) {
            if let Some(block) = self.block_tile(offset, current_tile.current_value().unwrap()) {
                blocked.push(block);
            }
        }

        blocked
    }

    fn block_square(&mut self, current_tile: &mut TestTile) -> Vec<TileBlock> {
        let mut blocked = vec!();
        // TODO do this better
        let s1: Vec<usize> = vec!(0,1,2,9,10,11,18,19,20);
        let s2: Vec<usize> = vec!(3,4,5,12,13,14,21,22,23);
        let s3: Vec<usize> = vec!(6,7,8,15,16,17,24,25,26);
        let s4: Vec<usize> = vec!(27,28,29,36,37,38,45,46,47);
        let s5: Vec<usize> = vec!(30,31,32,39,40,41,48,49,50);
        let s6: Vec<usize> = vec!(33,34,35,42,43,44,51,52,53);
        let s7: Vec<usize> = vec!(54,55,56,63,64,65,72,73,74);
        let s8: Vec<usize> = vec!(57,58,59,66,67,68,75,76,77);
        let s9: Vec<usize> = vec!(60,61,62,69,70,71,78,79,80);
        let s = vec!(s1,s2,s3,s4,s5,s6,s7,s8,s9);

        let index = current_tile.index();
        for square in s {
            if square.contains(&index) {
                for v in square {
                    if v > index {
                        // println!("Blocking {}", v);
                        if let Some(block) = self.block_tile(v, current_tile.current_value().unwrap()) {
                            blocked.push(block);
                        }
                    }
                }
                break;
            }
        }

        blocked
    }

    fn unblock_values(&mut self, blocking_values: &mut Vec<TileBlock>) {
        for block in blocking_values {
            self.board.get_mut(block.index()).unwrap().insert_possible_value(block.value());
        }
    }

     fn print(&self) {
        for y in 0..9 {
           let mut row: String = String::new();
           for x in 0..9 {
              let value = self.board.get(y*9 + x).unwrap().current_value();
              match value {
                  Some(v) => row.push_str(&v.to_string()),
                  None => row.push('x')
              };
              row.push(' ');
           }
           println!("{}", row);
        }
     }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_numgroup() {
        use crate::board::Board;
        use crate::tile::TestTile;

        let board = Board::new();
        let solver = Solver::new(&board.numgroups());

        let numgroup = solver.get_numgroup(&TestTile::new(0));
        assert_eq!(numgroup.unwrap().indexes().get(0).unwrap(), board.numgroups().get(0).unwrap().indexes().get(0).unwrap());
        assert_eq!(numgroup.unwrap().indexes().get(1).unwrap(), board.numgroups().get(0).unwrap().indexes().get(1).unwrap());
        assert_eq!(numgroup.unwrap().indexes().get(2).unwrap(), board.numgroups().get(0).unwrap().indexes().get(2).unwrap());
        assert_eq!(numgroup.unwrap().sum(), board.numgroups().get(0).unwrap().sum());

        let numgroup = solver.get_numgroup(&TestTile::new(80));
        assert_eq!(numgroup.unwrap().indexes().get(0).unwrap(), board.numgroups().get(28).unwrap().indexes().get(0).unwrap());
        assert_eq!(numgroup.unwrap().indexes().get(1).unwrap(), board.numgroups().get(28).unwrap().indexes().get(1).unwrap());
        assert_eq!(numgroup.unwrap().indexes().get(2).unwrap(), board.numgroups().get(28).unwrap().indexes().get(2).unwrap());
        assert_eq!(numgroup.unwrap().sum(), board.numgroups().get(28).unwrap().sum());
    }

    #[test]
    fn check_numgroup() {
        use crate::board::Board;
        use crate::tile::TestTile;

        let board = Board::new();
        let mut solver = Solver::new(&board.numgroups());

        let numgroup = solver.get_numgroup(&TestTile::new(0)).unwrap().clone();

        solver.board.get_mut(0).unwrap().set_current_value(1);
        let mut tile = TestTile::new(1);
        tile.set_current_value(7); 
        assert_eq!(solver.check_numgroup(&numgroup, &tile), true);

        solver.board.get_mut(1).unwrap().set_current_value(7);
        tile = TestTile::new(9);
        tile.set_current_value(8);
        assert_eq!(solver.check_numgroup(&numgroup, &tile), true);

        tile.set_current_value(10);
        assert_eq!(solver.check_numgroup(&numgroup, &tile), false);
    }
}