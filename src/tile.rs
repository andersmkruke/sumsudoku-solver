pub struct Tile {
   value: u8
}

impl Tile {
   pub fn new(value: u8) -> Self {
      if value == 0 || value > 9 {
         panic!("Tile value must be between 1 and 9 inclusive!");
      }
      Tile {
         value
      }
   }

   pub fn value(&self) -> u8 {
      self.value
   }

   pub fn set(&mut self, value: u8) {
      self.value = value
   }
}

#[derive(Clone)]
pub struct TileBlock {
   value: u8,
   index: usize,
}

impl TileBlock {
   pub fn new(value: u8, index: usize) -> Self {
      TileBlock {
         value,
         index,
      }
   }

   pub fn value(&self) -> u8 {
      self.value
   }

   pub fn index(&self) -> usize {
      self.index
   }
}

#[derive(Clone, Debug)]
pub struct TestTile {
   index: usize,
   current_value: Option<u8>,
   possible_values: Vec<u8>,
}

impl TestTile {
   pub fn new(index: usize) -> Self {
      TestTile {
         index,
         current_value: None,
         possible_values: vec!(1,2,3,4,5,6,7,8,9),
      }
   }

   pub fn index(&self) -> usize {
      self.index
   }

   pub fn current_value(&self) -> Option<u8> {
      self.current_value
   }

   pub fn set_current_value(&mut self, value: u8){
      self.current_value = Some(value);
   }

   pub fn clear_current_value(&mut self) {
      self.current_value = None;
   }

   pub fn possible_values(&self) -> &Vec<u8> {
      &self.possible_values
   }

   pub fn remove_possible_value(&mut self, value: u8) -> bool {
       for i in 0..self.possible_values().len() {
          if self.possible_values.get(i).unwrap() == &value {
             self.possible_values.remove(i);
             return true;
          }
       }
       return false;
   }

   pub fn insert_possible_value(&mut self, value: u8) -> bool {
      let search = self.possible_values.binary_search(&value);
      match search {
         Ok(_) => false,
         Err(_) => {
            self.possible_values.push(value);
            true
         }
      }
   }
}