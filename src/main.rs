mod board;
mod tile;
mod numgroup;
mod solver;

use board::Board;

use std::time::SystemTime;

fn main() {
    // let mut board = Board::new();

    let start_parse_time = SystemTime::now();
    let mut board = Board::from_file();
    let start_solve_time = SystemTime::now();
    board.solve();
    let end_time = SystemTime::now();
    println!("");
    println!("Problem parsed in {} microseconds", start_solve_time.duration_since(start_parse_time).unwrap().as_micros());
    println!("Solved in {} milliseconds", end_time.duration_since(start_solve_time).unwrap().as_millis());
}
